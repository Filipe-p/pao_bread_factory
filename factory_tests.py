# Import function & run tests 
from factory_functions import *

# What is a test? 
# It's an assertion, with a know outcome of a function that comes back with True or False 


# **User Story 1** AS a bread maker, I want to provide `flour` and `water` to my `make_dough_option` and get out `dough`, else I want `not dough`. So that I can then bake the bread.
print("test number 1 - make_dough_option with 'flour' and 'water' should equal 'dough'")

print(make_dough_option('flour', 'water') == 'dough')

print("test number 2 - make_dough_option with 'cement' and 'water' should equal 'not dough'")

print(make_dough_option('cement', 'water') == 'not dough')

# user story 2 + make tests + functions


# User story 3 + make tests + functions