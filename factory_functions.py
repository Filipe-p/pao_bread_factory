# Define functions (not for running and calling)

def make_dough_option(arg1, arg2):
    if 'flour' in arg1 and 'water' in arg2:
        return 'dough'
    else:
        return 'not dough'